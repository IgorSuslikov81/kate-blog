import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Header from './Header/Header.js'
import Main from './Main/Main'

const App = () => {
    return (
        <>
            <CssBaseline />
            <Header />
            <Main />
        </>
    )
}

export default App
