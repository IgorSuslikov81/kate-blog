import React from 'react'
import './Header.css'
import './Grid.css'

const Header = () => {
    return (
        <header className="header">
            <div className="container">
                <div className="row header-row">
                    <div className="col-xs-12 col-sm-5 col-md-5">
                        <div className="logo">
                            <a href="">
                                <img
                                    src="images/Logo.png"
                                    alt=""
                                    className="logo-img"
                                />
                            </a>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-5 col-md-5">
                        <nav className="headMenu">
                            <ul className="row-headMenu">
                                <li className="home">HOME</li>
                                <li className="about">HOW WE WORK</li>
                                <li className="work">BENEFITS</li>
                                <li className="team">OFFER</li>
                                <li className="services">CONTACT US</li>
                                <li className="features">BUY NOW</li>
                            </ul>
                        </nav>
                        <div className="nav-mobile-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div className="col-xs-hidden col-sm-2 col-md-2">
                        <div className="phone">
                            <a href="">
                                <div className="row row-phone">
                                    <img src="images/Phone-logo.png" alt="" />
                                    <h4 className="number-phone">
                                        +38(095)181-78-72
                                    </h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header
