import React from 'react'
import './Main.css'
import './Grid.css'

const Main = () => {
    return (
        <>
            <main className="main">
                <div className="welcome">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="row row-welcome">
                                    <div className="row-point">
                                        <div className="point-icon">
                                            <img
                                                src="images/circle.png"
                                                alt=""
                                            />
                                        </div>
                                        <p className="point-description">
                                            welcome
                                        </p>
                                    </div>
                                    <div className="description">
                                        <span>Financial </span>
                                        and <span>marketing</span> advisory
                                    </div>
                                    <div className="button-description">
                                        <button>CHECK MY OFFER</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="photo-hi">
                                    <img src="images/welcome.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="about-us">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="row row-welcome">
                                    <div className="row-point">
                                        <div className="point-icon">
                                            <img
                                                src="images/circle.png"
                                                alt=""
                                            />
                                        </div>
                                        <p className="point-description">
                                            about us
                                        </p>
                                    </div>
                                    <div className="description-about-us">
                                        We are an online consulting team
                                    </div>
                                    <div className="button-description">
                                        <button>READ MORE</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="row row-about-all">
                                    <div className="row-about-1">
                                        <div className="deep-knowledge">
                                            <img
                                                src="images/consultant-home-columnbg1.jpg"
                                                alt=""
                                                className="photo-deep"
                                            />
                                            <div className="about-inside-img">
                                                <div className="icon-deep">
                                                    <img
                                                        src="images/consult.png"
                                                        alt=""
                                                        className="icon-deep-in"
                                                    />
                                                </div>
                                                <div className="about-inside-img-text">
                                                    DEEP KNOWLEDGE OF MARKETING
                                                </div>
                                            </div>
                                        </div>
                                        <div className="years-of-expirience">
                                            <h2 className="about-number">12</h2>
                                            <h4 className="about-number-under">
                                                YEARS OF EXPERIENCE
                                            </h4>
                                        </div>
                                    </div>
                                    <div className="row-about-2">
                                        <div className="happy-client">
                                            <h2 className="about-number">
                                                13K
                                            </h2>
                                            <h4 className="about-number-under">
                                                HAPPY CLIENTS
                                            </h4>
                                        </div>
                                        <div className="deep-knowledge">
                                            <img
                                                src="images/consultant-home-columnbg2.jpg"
                                                alt=""
                                                className="photo-deep"
                                            />
                                            <div className="about-inside-img">
                                                <div className="icon-deep">
                                                    <img
                                                        src="images/consult.png"
                                                        alt=""
                                                        className="icon-deep-in"
                                                    />
                                                </div>
                                                <div className="about-inside-img-text">
                                                    BETHEME SERTIFICATE
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="marketing-facts">
                    <div className="container container-marketing-facts ">
                        <div className="row ">
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="photo-hi">
                                    <img
                                        src="images/consultant-home-pic1.jpg"
                                        alt=""
                                    />
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-6 col-md-6">
                                <div className="row row-welcome row-marketing">
                                    <div className="row-point">
                                        <div className="point-icon">
                                            <img
                                                src="images/circle.png"
                                                alt=""
                                            />
                                        </div>
                                        <p className="point-description">
                                            welcome
                                        </p>
                                    </div>
                                    <div className="description">
                                        Marketing facts.{' '}
                                        <span>
                                            Few words about our background
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-6 col-md-12">
                            <div className="row row-facts">
                                <div className="facts-1">
                                    <div className="paragraph-1">
                                        Lorem ipsum dolor sit amet enim. Etiam
                                        ullamcorper
                                    </div>
                                    <div className="paragraph-2">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Ut elit tellus, luctus
                                        nec mattis pulvinar leo.
                                    </div>
                                </div>
                                <div className="facts-2">
                                    <div className="paragraph-1">
                                        Lorem ipsum dolor sit amet enim. Etiam
                                        ullamcorper
                                    </div>
                                    <div className="paragraph-2">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Ut elit tellus, luctus
                                        nec mattis pulvinar leo.
                                    </div>
                                </div>
                                <div className="facts-3">
                                    <div className="paragraph-1">
                                        Lorem ipsum dolor sit amet enim. Etiam
                                        ullamcorper
                                    </div>
                                    <div className="paragraph-2">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Ut elit tellus, luctus
                                        nec mattis pulvinar leo.
                                    </div>
                                </div>
                                <div className="facts-4">
                                    <div className="paragraph-1">
                                        Lorem ipsum dolor sit amet enim. Etiam
                                        ullamcorper
                                    </div>
                                    <div className="paragraph-2">
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. Ut elit tellus, luctus
                                        nec mattis pulvinar leo.
                                    </div>
                                </div>
                            </div>
                            <div className="button-description button-marketing">
                                <button>SEE HOW IT WORKS </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="soft-skills">
                    <div className="container">
                        <div className="row"></div>
                    </div>
                </div>
                <div className="benefits">WHAT CAN YOU</div>
                <div className="contact">CONTACT US</div>
            </main>
        </>
    )
}

export default Main
